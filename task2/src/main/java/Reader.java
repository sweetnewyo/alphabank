import java.io.*;
import java.nio.charset.StandardCharsets;
import java.nio.file.Files;
import java.nio.file.Paths;
import java.util.Optional;

public class Reader {
    public Optional<String> readFirstFile(String path) throws IOException {
        return Files.lines(Paths.get(path), StandardCharsets.UTF_8).findFirst();
    }
}
