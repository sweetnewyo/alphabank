import java.io.IOException;
import java.util.ArrayList;
import java.util.Optional;

public class Solution {
    public static void main(String[] args) {
        String s;
        Reader r = new Reader();
        try {
            Optional<String> strOpt = r.readFirstFile("file.txt");
            s = strOpt.orElse("file is empty");
            String[] str = s.split(",");
            ArrayList<Integer> arr = new ArrayList<>();
            for (int i = 0; i < str.length; i++) {
                arr.add(Integer.parseInt(str[i]));
            }
            System.out.println(sort(arr,true));
            System.out.println(sort(arr,false));
        } catch (IOException e) {
            e.printStackTrace();
        }
    }


    public static ArrayList<Integer> sort(ArrayList<Integer> arrayList, boolean as) {
        for (int i = arrayList.size() - 1; i > 0; i--) {
            for (int j = 0; j < i; j++) {
                if (as) {
                    if (arrayList.get(j) > arrayList.get(j + 1)) {
                        int t = arrayList.get(j);
                        arrayList.set(j, arrayList.get(j + 1));
                        arrayList.set(j + 1, t);
                    }
                } else {
                    if (arrayList.get(j) < arrayList.get(j + 1)) {
                        int t = arrayList.get(j);
                        arrayList.set(j, arrayList.get(j + 1));
                        arrayList.set(j + 1, t);
                    }
                }
            }
        }
        return arrayList;
    }
}
